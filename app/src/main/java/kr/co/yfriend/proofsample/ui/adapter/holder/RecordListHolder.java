package kr.co.yfriend.proofsample.ui.adapter.holder;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import org.json.JSONException;

import kr.co.yfriend.proofsample.R;
import kr.co.yfriend.proofsample.model.database.Record;
import kr.co.yfriend.proofsample.util.CommonUtil;

/**
 * ProofSample
 * Class: RecordListHolder
 * Created by 한경동 (Joel) on 2022/03/21.
 * Description:
 */
public class RecordListHolder extends RecyclerView.ViewHolder {

    private ViewGroup vgItem;
    private ImageView ivThumbnail;
    private TextView tvIndex;
    private TextView tvDate;
    private TextView tvDesc;
    private TextView tvName;
    private View vDelete;

    public interface OnItemClickListener {
        void onItemClick(int position) throws JSONException;

        void onDeleteClick(int position);
    }


    private OnItemClickListener listener;

    public void setListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public RecordListHolder(@NonNull View itemView) {
        super(itemView);
        vgItem = itemView.findViewById(R.id.vg_item);
        ivThumbnail = itemView.findViewById(R.id.iv_thumbnail);
        tvIndex = itemView.findViewById(R.id.tv_index);
        tvDate = itemView.findViewById(R.id.tv_date);
        tvDesc = itemView.findViewById(R.id.tv_desc);
        tvName = itemView.findViewById(R.id.tv_name);
        vDelete = itemView.findViewById(R.id.v_delete);

    }


    public void onBind(Record record) {

        if (null != record) {

            tvIndex.setText(String.valueOf(getAdapterPosition() + 1));

            if (record.thumbNail != null) {
                // Bitmap bitmap = ImageUtils.stringToBitmap(record.thumbNail);
                Glide.with(itemView).load(record.thumbNail).apply(new RequestOptions().circleCrop()).into(ivThumbnail);
            }

            if (null != record.userName) {
                tvName.setText(record.userName);
            }

            if (null != record.createdAt) {
                tvDate.setText(CommonUtil.getDateFormatToString(record.createdAt, 0) + " " + CommonUtil.getDateFormatToString(record.createdAt, 1));
            }

            vgItem.setOnClickListener(v -> {
                // View 전체 버튼 Click 리스너
                if (listener != null) {
                    try {
                        listener.onItemClick(getAdapterPosition());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

            vDelete.setOnClickListener(v -> {
                // 삭제 버튼 Click 리스너
                if (listener != null) listener.onDeleteClick(getAdapterPosition());
            });

        }

    }

}

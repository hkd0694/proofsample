package kr.co.yfriend.proofsample.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import kr.co.yfriend.proofsample.R;
import kr.co.yfriend.proofsample.model.HistoryDto;
import kr.co.yfriend.proofsample.proofread.PRConfig;
import kr.co.yfriend.proofsample.proofread.PrDrawerSyncDataTest;
import kr.co.yfriend.proofsample.ui.component.PRViewTest;
import kr.co.yfriend.proofsample.util.CommonUtil;

public class ProofReadingTestActivity extends BaseActivity {

    private String videoUrl;
    private String thumbNail;
    private String proofReading;
    private long totalTimer;
    private ArrayList<HistoryDto> pathData;

    private ImageView ivFrame;
    private ImageView ivPlay;
    private ImageView ivPause;
    private PRViewTest prView;

    private TextView tvProgress;
    private SeekBar seekBar;
    private PrDrawerSyncDataTest syncData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_proof_reading_test);
        initView();
        Glide.with(this).load(thumbNail).into(ivFrame);
        setListener();
        if (syncData == null) activeSyncView(proofReading, videoUrl);
    }

    private void initView() {
        ivFrame = findViewById(R.id.iv_frame);
        ivPlay = findViewById(R.id.iv_record_play);
        ivPause = findViewById(R.id.iv_record_pause);
        seekBar = findViewById(R.id.seekbar);
        tvProgress = findViewById(R.id.tv_progress);
        prView = findViewById(R.id.pr_view);
        prView.setPaintInit();
        pathData = new ArrayList<>();
        Intent intent = getIntent();
        videoUrl = intent.getStringExtra("videoUrl");
        thumbNail = intent.getStringExtra("thumbNail");
        proofReading = intent.getStringExtra("proof");
        totalTimer = intent.getLongExtra("timer", 0);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (PRConfig.RECORD_PLAY_STATE) {
            this.syncData.stopSyncView();
        }
    }

    // setListener
    private void setListener() {
        ivPlay.setOnClickListener(v -> {
            // vgPlay.setVisibility(View.GONE);
            prView.setVisibility(View.VISIBLE);
            // 저장한 첨삭 Start
            activeSyncPlay();
            isProofShowView(true);
        });
        ivPause.setOnClickListener(v -> {
            if (syncData != null) syncData.pauseSyncView();
            isProofShowView(false);
        });
        seekBar.setMax((int) totalTimer);
        seekBar.setProgress(0);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                // if(syncData != null) syncData.setAudioSeekTo(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private void isProofShowView(boolean isCheck) {
        if (isCheck) {
            showViews(ivPause);
            hideViews(ivPlay);
        } else {
            showViews(ivPlay);
            hideViews(ivPause, prView);
        }
    }

    /**
     * 저장한 첨삭 내용 Setting
     *
     * @param drawData 첨삭데이터 String 형태
     * @param mp4Path  음성 파일
     */
    private void activeSyncView(String drawData, String mp4Path) {
        try {
            ArrayList currentData = (ArrayList) CommonUtil.getSerializationObjectToString(drawData);
            if (mp4Path != null) {
                syncData = new PrDrawerSyncDataTest(this, currentData, mp4Path, prView);
            } else {
                syncData = new PrDrawerSyncDataTest(this, currentData, prView);
            }
            syncData.setOnSyncEventListener(new PrDrawerSyncDataTest.OnSyncEventListener() {
                @Override
                public void onCompleteSync() {
                    // 저장된 첨삭을 모두 보여줬을 때
                    // vgPlay.setVisibility(View.VISIBLE);
                    prView.btnEraser(2);
                    seekBar.setProgress(0);
                    isProofShowView(false);
                    PRConfig.RECORD_PLAY_STATE = false;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // 첨삭 기록된 내용 Start Method
    private void activeSyncPlay() {
        if (syncData != null)
            syncData.startSyncView();
        PRConfig.RECORD_PLAY_STATE = true;
    }

}
package kr.co.yfriend.proofsample.model.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

/**
 * android
 * Class: RecordDao
 * Created by 한경동 (Joel) on 2021/10/10.
 * Description:
 */
@Dao
public interface RecordDao {
    @Query("SELECT * FROM record")
    List<Record> getAll();

    @Query("SELECT COUNT(*) FROM record")
    int getCount();

    @Insert
    void insertAll(Record... records);

    @Delete
    void delete(Record record);
}

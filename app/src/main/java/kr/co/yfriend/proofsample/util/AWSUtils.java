package kr.co.yfriend.proofsample.util;

import static android.os.Environment.DIRECTORY_DOWNLOADS;
import static kr.co.yfriend.proofsample.CommonConstant.ACCESS_KEY;
import static kr.co.yfriend.proofsample.CommonConstant.DEFAULT_BUCKET_NAME;
import static kr.co.yfriend.proofsample.CommonConstant.S3_HOST;
import static kr.co.yfriend.proofsample.CommonConstant.S3_SCHEME;
import static kr.co.yfriend.proofsample.CommonConstant.SECRET_KEY;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferNetworkLossHandler;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import kr.co.yfriend.proofsample.callback.AWSUploadCallback;

/**
 * ProofSample
 * Class: AWSUtils
 * Created by 한경동 (Joel) on 2022/03/21.
 * Description:
 */
public class AWSUtils {

    private static final int BUFFER = 80000;
    private static final int BUFFER_SIZE = 1024 * 2;
    public static final String TAG = AWSUtils.class.getSimpleName();

    private AWSUploadCallback callback;

    private Context context;

    private String filePath = "";

    private String bucket;
    private String preBucket;
    private Bitmap thumbnail;

    private TransferUtility transferUtility;

    private CognitoCachingCredentialsProvider cognitoCachingCredentialsProvider;

    public void setCallback(AWSUploadCallback callback) {
        this.callback = callback;
    }

    public AWSUtils(Context context) {
        this.context = context;
        // Amazon Cognito 인증 공급자를 초기화 진행
        // cognitoCachingCredentialsProvider = new CognitoCachingCredentialsProvider(
        //         context,
        //         DEFAULT_COGNITO_CREDENTIAL, // AWS Cognito 자격 증명 풀 ID 값
        //         Regions.AP_NORTHEAST_2      // 리전
        // );
    }

    public Bitmap getThumbNail() {
        return thumbnail;
    }

    /**
     * Bitmap 이미지를 파일로 저장 후 경로를 Return 해준다.
     * @param image Bitmap
     * @param index Image index
     * @return
     */
    public String onBitmapConvertFilePath(Bitmap image, int index) {
        String timestamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String path = "IMAGE_" + timestamp + "_" + index + ".jpeg";
        if(index == 0) thumbnail = image;
        OutputStream out;
        File file = null;
        try {
            // 파일 캐시 영역에 저장 한다.
            file = new File(context.getExternalCacheDir(), path);
            if (!file.exists()) file.createNewFile();
            out = new FileOutputStream(file);
            // 여기서 Image Resizing 필요
            // quality 를 줄이면 이미지 용량이 줄어든다..
            // 2021/01/24 : quality
            // 95 -> 90 으로 줄임
            image.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return file.getAbsolutePath();
    }

    /**
     * Bitmap 이미지를 파일로 저장 후 경로를 Return 해준다.
     * @param image Bitmap
     * @param index Image index
     * @return
     */
    public String onBitmapConvertFilePathAbs(Bitmap image, int index) {
        String timestamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String path = "IMAGE_" + timestamp + "_" + index + ".jpeg";
        // if(index == 0) thumbnail = image;
        OutputStream out;
        File file = null;
        try {
            // 파일 캐시 영역에 저장 한다.
            file = new File(context.getFilesDir(), path);
            this.filePath = file.getAbsolutePath();
            if (!file.exists()) file.createNewFile();
            out = new FileOutputStream(file);
            // 여기서 Image Resizing 필요
            // quality 를 줄이면 이미지 용량이 줄어든다..
            image.compress(Bitmap.CompressFormat.JPEG, 80, out);
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return file.getAbsolutePath();
    }


    /**
     * Zip File 만드는 Method
     * @param paths 캐시에 저장되어 있는 Image File List
     * @return
     */
    public String uploadZip(Context context, List<String> paths) {

        // File fileDataBase = context.getFilesDir();
        File fileDataBase = Environment.getExternalStoragePublicDirectory(DIRECTORY_DOWNLOADS);
        if (!fileDataBase.exists()) {
            fileDataBase.mkdirs();
        }

        String timestamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());

        String filePath = fileDataBase.getPath() + "/" + timestamp + ".zip";

        File vidFile = new File(filePath);

        // File file = new File(context.getCacheDir(), prepend);

        // this.filePath = vidFile.getAbsolutePath();
        try {
            BufferedInputStream origin;
            FileOutputStream dest = new FileOutputStream(filePath);
            ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(dest));
            byte data[] = new byte[BUFFER];
            int len = paths.size();
            for (int i = 0; i < len; i++) {
                FileInputStream fi = new FileInputStream(paths.get(i));
                origin = new BufferedInputStream(fi, BUFFER);
                ZipEntry entry = new ZipEntry(paths.get(i).substring(paths.get(i).lastIndexOf("/") + 1));
                out.putNextEntry(entry);
                int count;
                while ((count = origin.read(data, 0, BUFFER)) != -1) {
                    out.write(data, 0, count);
                }
                origin.close();
            }
            out.close();
            return filePath;
        } catch (Exception e) {
            return "fail";
        }
    }

    public void awsUploadAudioFile(String bucketPath, String path) {
        // IAM 설정 후 키를 생성하면 ACCESS_KEY, SECRET_KEY 생성
        AWSCredentials awsCredentials = new BasicAWSCredentials(ACCESS_KEY, SECRET_KEY);

        TransferNetworkLossHandler.getInstance(context);

        AmazonS3Client client = new AmazonS3Client(awsCredentials, Region.getRegion(Regions.AP_NORTHEAST_2));

        preBucket = bucketPath;
        // remofittest/analyze -> 이 경로에다 이미지 리스트 저장할 예정
        bucket = DEFAULT_BUCKET_NAME + bucketPath;

        transferUtility = TransferUtility.builder()
                .context(context)
                /*
                 * Default Bucket Path
                 * 이미지 Array      -> DEFAULT_BUCKET_NAME + "/analyze"
                 */
                .defaultBucket(bucket)
                .s3Client(client)
                .build();

        new Handler().post(() -> {

            String fileName = "한경동" + "_" + new SimpleDateFormat("yyyyMMdd_HHmmssSSS").format(new Date()) + ".mp4";
            File file = new File(path);
            // S3 에 올라갈 파일명
            TransferObserver observer = transferUtility.upload(
                    fileName,
                    file,
                    CannedAccessControlList.PublicRead
            );
            observer.setTransferListener(new TransferListener() {
                @Override
                public void onStateChanged(int id, TransferState state) {
                    if (state == TransferState.COMPLETED) {
                        // 업로드가 성공적으로 진행되었을 경우
                        if (callback != null) {
                            // DB 에 들어갈 파일 명
                            String fullPath = S3_SCHEME + DEFAULT_BUCKET_NAME + S3_HOST + preBucket + File.separator + fileName;
                            Log.d(TAG, fullPath);
                            callback.onSuccess(fullPath, null);
                        }
                    }
                }

                @Override
                public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                    if (callback != null) callback.onProgressChange();
                }

                @Override
                public void onError(int id, Exception ex) {
                    if (callback != null) callback.onError(ex);
                }
            });
        });

    }


    /**
     * AWS S3 파일을 업로드 하는 메서드
     *
     * @param bucketPath "/analyze" ( 여기서는 경로가 한개여서 굳이 매개변수로 안받아도 되지만 또 생길지 몰라 우선 받음 )
     */
    public void awsUploadTransferUtility(String bucketPath) {
        // IAM 설정 후 키를 생성하면 ACCESS_KEY, SECRET_KEY 생성
        AWSCredentials awsCredentials = new BasicAWSCredentials(ACCESS_KEY, SECRET_KEY);

        TransferNetworkLossHandler.getInstance(context);

        AmazonS3Client client = new AmazonS3Client(awsCredentials, Region.getRegion(Regions.AP_NORTHEAST_2));

        preBucket = bucketPath;
        // remofittest/analyze -> 이 경로에다 이미지 리스트 저장할 예정
        bucket = DEFAULT_BUCKET_NAME + bucketPath;

        transferUtility = TransferUtility.builder()
                .context(context)
                /*
                 * Default Bucket Path
                 * 이미지 Array      -> DEFAULT_BUCKET_NAME + "/analyze"
                 */
                .defaultBucket(bucket)
                .s3Client(client)
                .build();

        new Handler().post(() -> {

            String fileName = "한경동" + "_" + new SimpleDateFormat("yyyyMMdd_HHmmssSSS").format(new Date()) + ".JPEG";
            File file = new File(this.filePath);
            // S3 에 올라갈 파일명
            TransferObserver observer = transferUtility.upload(
                    fileName,
                    file,
                    CannedAccessControlList.PublicRead
            );
            observer.setTransferListener(new TransferListener() {
                @Override
                public void onStateChanged(int id, TransferState state) {
                    if (state == TransferState.COMPLETED) {
                        // 업로드가 성공적으로 진행되었을 경우
                        if (callback != null) {
                            // DB 에 들어갈 파일 명
                            String fullPath = S3_SCHEME + DEFAULT_BUCKET_NAME + S3_HOST + preBucket + File.separator + fileName;
                            Log.d(TAG, fullPath);
                            callback.onSuccess(fullPath, thumbnail);
                        }
                    }
                }

                @Override
                public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                    if (callback != null) callback.onProgressChange();
                }

                @Override
                public void onError(int id, Exception ex) {
                    if (callback != null) callback.onError(ex);
                }
            });
        });
    }

    /**
     * S3 에 올라가져 있는 Zip 파일을 다운로드 한다.
     * @param url
     * @param path
     * @return
     */
    public boolean downLoadImageUrl(String url, String path) {
        int count;
        try {
            URL urlTest = new URL(url);
            URLConnection connection = urlTest.openConnection();
            connection.connect();
            InputStream input = new BufferedInputStream(urlTest.openStream());
            OutputStream outputStream = new FileOutputStream(path);
            byte[] data = new byte[BUFFER];
            long total = 0;
            while((count = input.read(data)) != -1 ) {
                total += count;
                outputStream.write(data, 0, count);
            }
            outputStream.close();
            input.close();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * S3 에 올라가 있는 Zip 파일을 다운 후, File 에 저장 후 다시 Bitmap List 에 저장 한다.
     * @param cPath
     * @return
     */
    public List<Bitmap> unZip(String cPath) {
        InputStream is;
        ZipInputStream zis;
        List<Bitmap> bitmaps = new ArrayList<>();
        try {
            File zipFile = new File(cPath);
            String parentFolder = zipFile.getParentFile().getPath();
            String filename;
            is = new FileInputStream(cPath);
            zis = new ZipInputStream(new BufferedInputStream(is));
            ZipEntry ze;
            byte[] buffer = new byte[BUFFER_SIZE];
            int count;
            while ((ze = zis.getNextEntry()) != null) {
                filename = ze.getName();
                String path = parentFolder + "/" + filename;
                File deletePath = new File(path);
                if (ze.isDirectory()) {
                    File fmd = new File(parentFolder + "/" + filename);
                    fmd.mkdirs();
                    continue;
                }
                FileOutputStream fout = new FileOutputStream(path);
                while ((count = zis.read(buffer)) != -1) {
                    fout.write(buffer, 0, count);
                }
                Bitmap bitmap = BitmapFactory.decodeFile(path);
                bitmaps.add(bitmap);
                deletePath.delete();
                fout.close();
                zis.closeEntry();
            }
            zis.close();
            return bitmaps;
        } catch (Exception e) {
            return null;
        }
    }

}

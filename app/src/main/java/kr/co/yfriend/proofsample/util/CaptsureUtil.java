package kr.co.yfriend.proofsample.util;

import static android.os.Environment.DIRECTORY_DOWNLOADS;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Environment;
import android.view.View;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * ProofSample
 * Class: CaptureUtil
 * Created by 한경동 (Joel) on 2022/03/21.
 * Description:
 */
public class CaptsureUtil {

    public static String captureView(View view) {
        Bitmap captureView = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(captureView);
        view.draw(canvas);

        File thumbnail = Environment.getExternalStoragePublicDirectory(DIRECTORY_DOWNLOADS);

        String timestamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());

        String path = "IMAGE_" + timestamp + ".jpeg";
        OutputStream out;
        File file = null;
        try {
            // 파일 캐시 영역에 저장 한다.
            file = new File(thumbnail.getPath(), path);
            if (!file.exists()) file.createNewFile();
            out = new FileOutputStream(file);
            // 여기서 Image Resizing 필요
            captureView.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return file.getAbsolutePath();
    }
}

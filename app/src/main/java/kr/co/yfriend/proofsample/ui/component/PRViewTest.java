package kr.co.yfriend.proofsample.ui.component;

import static kr.co.yfriend.proofsample.proofread.PRConfig.DRAWETYPE_ANGLE;
import static kr.co.yfriend.proofsample.proofread.PRConfig.DRAWTYPE_ERASER;
import static kr.co.yfriend.proofsample.proofread.PRConfig.DRAWTYPE_PEN;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.os.PowerManager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.Nullable;

import java.text.DecimalFormat;
import java.util.ArrayList;

import kr.co.yfriend.proofsample.model.CanvasLine;
import kr.co.yfriend.proofsample.model.HistoryDto;
import kr.co.yfriend.proofsample.proofread.PRConfig;

/**
 * ProofSample
 * Class: PRViewTest
 * Created by 한경동 (Joel) on 2022/03/25.
 * Description:
 */
public class PRViewTest extends View {

    String DRAWING_CANVAS_COLOR_RED = "#FFFF0000";
    String DRAWING_CANVAS_COLOR_BLUE = "#FF0000FF";
    String DRAWING_CANVAS_COLOR_YELLOW = "#FFFFFF00";
    String DRAWING_CANVAS_COLOR_WHITE = "#FFFFFFFF";

    private Paint mPencilPaint;     // Draw Paint
    public static Paint eraserPaint;// Eraser Paint
    public Canvas mCanvas;          // mCanvas
    Path savePath = new Path();     // savePath

    public Bitmap mBitmap;          // 그릴 이미지
    private Bitmap previousBitmap;  // 이전 이미지

    private String mDrawType;       // Draw Type
    private String color = DRAWING_CANVAS_COLOR_RED;

    Context context;

    float sxTest = 0.0F;
    float syTest = 0.0F;

    private int penState = 0;
    private boolean isFinish = false;       // state = 3 부터 true

    PowerManager pm;

    PowerManager.WakeLock wl;
    private boolean isChange = false;
    private boolean isEnable = true;
    public static boolean mPathException = false;
    HistoryDto saveData = new HistoryDto();             // History 저장 VO
    private ArrayList<CanvasLine> lines = new ArrayList<>();

    public Boolean mSyncInvalidateDisable = false;

    public PRViewTest(Context context) {
        super(context);
        this.context = context;
        initView();
    }

    public PRViewTest(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        initView();
    }

    public PRViewTest(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        initView();
    }

    public void setSyncInvalidateDisable(Boolean mSyncInvalidateDisable) {
        this.mSyncInvalidateDisable = mSyncInvalidateDisable;
    }

    @SuppressLint({"InvalidWakeLockTag", "WrongConstant"})
    private void initView() {
        if (pm == null) {
            pm = ((PowerManager) context.getSystemService("power"));
        }
        if (wl == null)
            wl = pm.newWakeLock(536870922, "My Tag");
        setPaintInit();
    }

    public void setPaintInit() {

        if (mPencilPaint == null) {
            mPencilPaint = new Paint();
        }

        if (eraserPaint == null) {
            eraserPaint = new Paint(1);
            eraserPaint.setStyle(Paint.Style.STROKE);
            eraserPaint.setStrokeJoin(Paint.Join.ROUND);
            eraserPaint.setStrokeCap(Paint.Cap.ROUND);
            eraserPaint.setAntiAlias(true);
            eraserPaint.setStrokeWidth(35);
            eraserPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        }

        mPencilPaint.reset();
        mPencilPaint.setStyle(Paint.Style.STROKE);
        mPencilPaint.setStrokeJoin(Paint.Join.ROUND);
        mPencilPaint.setStrokeCap(Paint.Cap.ROUND);
        mPencilPaint.setStrokeWidth(7);
        mPencilPaint.setColor(Color.parseColor(this.color));
        mPencilPaint.setAntiAlias(true);
        mPencilPaint.setMaskFilter(null);
        mPencilPaint.setPathEffect(null);
        mDrawType = DRAWTYPE_PEN;
    }

    public void setDrawType(String type) {
        this.mDrawType = type;
        if(mDrawType.equals(DRAWTYPE_ERASER)){
            mPencilPaint.setStrokeWidth(35);
            mPencilPaint.setStyle(Paint.Style.STROKE);
            mPencilPaint.setColor(Color.TRANSPARENT);
            mPencilPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        } else {
            mPencilPaint.setColor(Color.parseColor(this.color));
        }
    }

    public void setColor(String color) {
        this.color = color;
        mPencilPaint.setColor(Color.parseColor(this.color));
    }

    public void btnEraser(int type) {
        switch (type) {
            case 2:
                savePath.reset();
                eraserPaint.setStyle(Paint.Style.FILL);
                // 전체 지우기
                this.mCanvas.drawRect(0.0F, 0.0F, getWidth(), getHeight(), eraserPaint);
                invalidate();
                break;
        }
    }

    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        Bitmap.Config config = Bitmap.Config.ARGB_8888;
        Log.d("View SIZE CHANGE:", w + "x" + h + ":" + this.mBitmap);

        if (mBitmap == null) {
            mBitmap = Bitmap.createBitmap(w, h, config);
            mCanvas = new Canvas(mBitmap);
            initView();
        } else {
            mBitmap = Bitmap.createScaledBitmap(mBitmap, w, h, true);
            mCanvas = new Canvas(mBitmap);
        }
        invalidate();
    }

    protected void onDraw(Canvas canvas) {

        if (this.mSyncInvalidateDisable)
            return;
        if (mDrawType.equals(DRAWTYPE_ERASER)) {
            mCanvas.drawPath(savePath, mPencilPaint);
            canvas.drawBitmap(mBitmap, 0.0F, 0.0F, null);
        } else {
            canvas.drawBitmap(mBitmap, 0, 0, null);
            canvas.drawPath(savePath, mPencilPaint);
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (!this.isEnable)
            return true;
        Log.d("SCROLL", "X : " + event.getX() + "   Y: " + event.getY());

        float x = event.getX();
        float y = event.getY();

        setDrawingCacheEnabled(true);

        if (PRConfig.RECORD_PLAY_STATE) {
            return true;
        }

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                // 손을 터치 할때 ( 0 )
                PRConfig.undoData.clear();
                if (mDrawType.equals(DRAWTYPE_ERASER)) {
                    // 지우개 모드
                    saveData.setDrawType(111);
                    saveData.setSaveBlushSize(20);
                } else if (mDrawType.equals(DRAWTYPE_PEN)) {
                    // 그리기 모드
                    saveData.setDrawType(222);
                    saveData.setSaveBlushSize(7);
                }
                saveData.setSaveBlushColor(this.color.substring(1));
                saveData.setStartX(x);
                saveData.setStartY(y);

                if ((PRConfig.START_RECORD_TIME == 0L) && (!PRConfig.RECORD_AUDIO)) {
                    saveData.setStartTime(0.0F);
                } else {
                    // 기록 모드 일때 startTime 을 준다.
                    saveData.setStartTime((float) (System.currentTimeMillis() - PRConfig.START_RECORD_TIME));
                }

                if (mDrawType.equals(DRAWTYPE_PEN) || mDrawType.equals(DRAWTYPE_ERASER)) {
                    penState = 1;
                }

                mPathException = true;
                break;
            case MotionEvent.ACTION_MOVE:
                // 손을 움직 일때 ( 2 )
                if (mDrawType.equals(DRAWTYPE_PEN) || mDrawType.equals(DRAWTYPE_ERASER)) {
                    touchMove(x, y);
                }
                CanvasLine line = new CanvasLine();
                line.setPointX(x);
                line.setPointY(y);
                line.setPointTime(System.currentTimeMillis() - PRConfig.START_RECORD_TIME);
                lines.add(line);
                break;
            case MotionEvent.ACTION_UP:
                // 손을 뗄때 ( 1 )
                if (mDrawType.equals(DRAWTYPE_PEN) || mDrawType.equals(DRAWTYPE_ERASER)) {
                    mCanvas.drawPath(savePath, mPencilPaint);
                    savePath.reset();
                    penState = 3;
                }
                mPathException = false;
                saveData.setLines(lines);
                saveData.setEndX(x);
                saveData.setEndY(y);
                PRConfig.pathData.add(saveData);
                lines = new ArrayList<>();
                saveData = new HistoryDto();
                break;
        }
        return true;
    }

    // Touch Move
    private void touchMove(float x, float y) {
        if (!this.isChange)
            this.isChange = true;
        if (penState == 1) {
            savePath.moveTo(x, y);
            sxTest = x;
            syTest = y;
        }
        savePath.quadTo(sxTest, syTest, (x + sxTest) / 2, (y + syTest) / 2);
        int iSizeWidth = 100;
        int cx = ((int) sxTest + (int) x) / 2;
        int cy = ((int) syTest + (int) y) / 2;
        if (!this.mSyncInvalidateDisable.booleanValue()) {
            invalidate(cx - iSizeWidth, cy - iSizeWidth, cx + iSizeWidth, cy + iSizeWidth);
        }
        sxTest = x;
        syTest = y;

        penState = 2;
    }


    public void setSaveDraw(HistoryDto historyDto) {
        switch (historyDto.getDrawType()) {
            case PRConfig.DRAW_PEN:
                setPaintInit();
                penState = 1;
                mPencilPaint.setColor(Color.parseColor("#" + historyDto.getSaveBlushColor()));
                mPencilPaint.setStrokeWidth(historyDto.getSaveBlushSize());
                ArrayList<CanvasLine> lines = historyDto.getLines();
                int length = lines.size();
                for(int i=0; i< length; i++) {
                    CanvasLine line = lines.get(i);
                    touchMove(line.getPointX(), line.getPointY());
                }
                mCanvas.drawPath(savePath, mPencilPaint);
                savePath.reset();
                penState = 3;
                break;
            case PRConfig.DRAW_ERASER:
                setPaintInit();
                mDrawType = DRAWTYPE_ERASER;
                mPencilPaint.setStrokeWidth(historyDto.getSaveBlushSize());
                mPencilPaint.setStyle(Paint.Style.STROKE);
                mPencilPaint.setXfermode(new PorterDuffXfermode(
                        PorterDuff.Mode.CLEAR));
                penState = 1;
                ArrayList<CanvasLine> linesData = historyDto.getLines();
                int lengthLine = linesData.size();
                for(int i=0; i< lengthLine; i++) {
                    CanvasLine line = linesData.get(i);
                    touchMove(line.getPointX(), line.getPointY());
                }
                mCanvas.drawPath(savePath, mPencilPaint);
                savePath.reset();
                penState = 3;
                break;
        }
    }

    public void setDrawStart(HistoryDto historyDto) {
        setPaintInit();
        penState = 1;
        if(historyDto.getDrawType() == PRConfig.DRAW_PEN) {
            mPencilPaint.setColor(Color.parseColor("#" + historyDto.getSaveBlushColor()));
            mPencilPaint.setStrokeWidth(historyDto.getSaveBlushSize());
        } else {
            mDrawType = DRAWTYPE_ERASER;
            mPencilPaint.setStrokeWidth(historyDto.getSaveBlushSize());
            mPencilPaint.setStyle(Paint.Style.STROKE);
            mPencilPaint.setXfermode(new PorterDuffXfermode(
                    PorterDuff.Mode.CLEAR));
        }
        ArrayList<CanvasLine> line = historyDto.getLines();
        if(line.size() > 1) {
            CanvasLine line1 = line.get(0);
            touchMove(line1.getPointX(), line1.getPointY());
        }
    }

    public void setDrawMove(float nX, float nY) {
        touchMove(nX, nY);
    }

    public void setDrawEnd() {
        mCanvas.drawPath(savePath, mPencilPaint);
        savePath.reset();

        penState = 3;
    }

}

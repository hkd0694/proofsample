package kr.co.yfriend.proofsample.model;

import java.io.Serializable;

/**
 * ProofSample
 * Class: CanvasLine
 * Created by 한경동 (Joel) on 2022/03/25.
 * Description:
 */
public class CanvasLine implements Serializable {

    private float pointX;       // Line 을 그려주는 X 좌표
    private float pointY;       // Line 을 그려주는 Y 좌표
    private long pointTime;          // Line 을 그려준 시간

    public CanvasLine() {

    }

    public CanvasLine(float pointX, float pointY, long pointTime) {
        this.pointX = pointX;
        this.pointY = pointY;
        this.pointTime = pointTime;
    }

    public float getPointX() {
        return pointX;
    }

    public void setPointX(float pointX) {
        this.pointX = pointX;
    }

    public float getPointY() {
        return pointY;
    }

    public void setPointY(float pointY) {
        this.pointY = pointY;
    }

    public long getPointTime() {
        return pointTime;
    }

    public void setPointTime(long pointTime) {
        this.pointTime = pointTime;
    }
}

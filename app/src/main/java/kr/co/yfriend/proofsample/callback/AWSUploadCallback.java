package kr.co.yfriend.proofsample.callback;

import android.graphics.Bitmap;

/**
 * android
 * Class: AWSUploadCallback
 * Created by 한경동 (Joel) on 2021/09/30.
 * Description:
 */
public interface AWSUploadCallback {
    // S3 인증 성공
    void onSuccess(String filePath, Bitmap thumbnail);
    // S3 인증 진행중
    void onProgressChange();
    // S3 인증 에러
    void onError(Exception ex);
}

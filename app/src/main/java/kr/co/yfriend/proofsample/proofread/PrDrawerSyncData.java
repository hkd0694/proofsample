package kr.co.yfriend.proofsample.proofread;

import android.app.Activity;
import android.content.Context;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;

import kr.co.yfriend.proofsample.model.HistoryDto;
import kr.co.yfriend.proofsample.ui.component.PRView;

/**
 * android
 * Class: PrDrawerSyncData
 * Created by 한경동 (Joel) on 2021/11/03.
 * Description:
 */
public class PrDrawerSyncData {

    private final String TAG = PrDrawerSyncData.class.getSimpleName();
    private static final String SYNC_STATE_PLAY = "play";
    private static final String SYNC_STATE_PAUSE = "pause";
    private static final String SYNC_STATE_STOP = "stop";
    private String mSyncState;                  // 첨삭 상태 ( play, stop, pause )
    private Activity mParent;                   // runUIThread 돌리기위한 Activity
    private ArrayList<HistoryDto> mSyncData;    // 첨삭 그림 도형 Data List
    private HistoryDto mHistory;                // HistoryDto 도형 단일 Data
    private ArrayList<Double> mArrXY;            // 0 : X 좌표, 1 : Y 좌표, 2 -> 시간
    private int currentHistoryType;             // 현재 그릴 도형 Type -> 222 그리기 모드

    private int currentNumHistory;              // 현재 첨삭을 그린 갯수
    private int cntHistoryCnt;                  // 첨삭 그려진 갯수
    private int cntHistorySubCnt;               // 움직일때마다 그려진 현재 좌표
    private int currentNumHistorySub;           // 총 움직일 좌표 갯수

    private PRView board;                       // Draw View
    float timeDiff = -1.0F;                     // 시간 차
    private MediaPlayer mPlayer;                // Audio Player
    private String mAudioPath;                  // Audio Path
    private boolean bAudioPlay = false;         // 음성파일 체크 유무 Boolean
    private long audioPlayTime;                 // Audio Play Time
    private OnSyncEventListener mSyncListener = null;   // 첨삭 종료 Listener

    public interface OnSyncEventListener {
        // 첨삭 완료 Method
        void onCompleteSync(int duration);
    }

    public void setOnSyncEventListener(OnSyncEventListener mSyncListener) {
        this.mSyncListener = mSyncListener;
    }

    public PrDrawerSyncData(Context context, ArrayList<HistoryDto> arrData, PRView prView) {
        this.mParent = ((Activity) context);
        this.mSyncData = arrData;
        this.board = prView;
    }

    public PrDrawerSyncData(Context context, ArrayList<HistoryDto> arrData, String audioPath, PRView prView) {
        this.mParent = ((Activity) context);
        this.mSyncData = arrData;
        this.mAudioPath = audioPath;
        this.board = prView;
    }

    public void pauseSyncView() {
        this.mSyncState = SYNC_STATE_PAUSE;
        if (this.mPlayer != null) {
            this.mPlayer.pause();
            // this.mPlayer.seekTo(mPlayer.getDuration());
        }
    }

    public void stopSyncView() {
        this.mSyncState = SYNC_STATE_STOP;
        this.board.btnEraser(2);
        if (this.mPlayer != null) {
            this.mPlayer.stop();
        }
    }

    public void clear() {
        this.mSyncState = SYNC_STATE_STOP;
        this.board.btnEraser(2);
    }

    public void setAudioSeekTo(int progress) {
        if(this.mPlayer != null) {
            this.mPlayer.pause();
            this.mPlayer.seekTo(progress);
            this.mPlayer.start();
        }
    }
    public void reStartAudio() {
        if(this.mPlayer != null) {
            this.mPlayer.start();
        }
    }

    /**
     * 저장된 데이터 첨삭 Start
     */
    public void startSyncView() {
        if ((this.mSyncState == null) || (!this.mSyncState.equals(SYNC_STATE_PAUSE))) {
            Log.d(this.TAG, "startSyncView");
            if ((this.mSyncData != null) && (this.mSyncData.size() > 0)) {
                this.cntHistoryCnt = this.mSyncData.size(); // 첨삭 그려진 갯수 초기화
                this.currentNumHistory = -1;    // 현재 첨삭을 그린 갯수 초기화
                this.cntHistorySubCnt = -1;     // 움직일때마다 그려진 현재 좌표 초기화
                this.currentNumHistorySub = -1; // 총 움직일 좌표 갯수 초기화
                this.mParent.runOnUiThread(() -> {
                    Log.d(TAG, "clear..call" + board.getId() + ":" + board);
                    // board.initView();
                    // 첨삭을 시작하기 전 View 전체 지우고 시작
                    // FIXME : 여기 주석 처리 했음..
                    // board.btnEraser(2);
                });
                // 첨삭 시간 차 초기화
                this.timeDiff = 0.0F;
            }
        }

        // 상태값 변경 stop -> play
        this.mSyncState = "play";

        // 전달받은 Path 가 있을 경우
        if (this.mAudioPath != null) {
            // MediaPlayer 음성파일 재생 Player
            this.mPlayer = new MediaPlayer();
            try {
                // 음성 파일 Path Setting
                this.mPlayer.setDataSource(this.mAudioPath);
            } catch (Exception localException) {}
            // 음성파일 재생 반복 여부 체크
            this.mPlayer.setLooping(false);
            this.mPlayer.setOnCompletionListener(mp -> {
                // 음성파일이 전부 틀어졌을 경우 Listener
                Log.e("SyncDrawView", "mediaplay complete:" + PrDrawerSyncData.this.mSyncListener);
                if (PrDrawerSyncData.this.mSyncListener != null)
                    // 완료 시 Listener 전달
                    PrDrawerSyncData.this.mSyncListener.onCompleteSync(mp.getDuration());
                // 첨삭 진행상태 상태값 변경 start -> stop
                PrDrawerSyncData.this.mSyncState = "stop";
                // 음성파일 재생 Player null 선언
                PrDrawerSyncData.this.mPlayer = null;
                // Record 상태 False
                PRConfig.RECORD_PLAYING_STATE = false;
            });
        }

        if (this.mPlayer != null) {
            this.bAudioPlay = false;
            try {
                // Player prepare
                this.mPlayer.prepare();
            } catch (IllegalStateException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            // 음성파일 없을 경우 바로 SetProgressTask 실행
            Log.d(this.TAG, "사운드 없음 모든 드로우 데이터 바로 그려넣기");
        }
        new SetProgressTask().execute();
    }

    /**
     * 첨삭을 시작하기 전 최초로 도형을 그리는 시간을 구해주기 위해 AsyncTask 실행
     */
    private class SetProgressTask extends AsyncTask<Void, Void, Float> {
        private SetProgressTask() {
        }

        protected void onPreExecute() {
            super.onPreExecute();
            PrDrawerSyncData.this.board.setSyncInvalidateDisable(true);
        }

        protected Float doInBackground(Void[] params) {
            // play 상태가 아닐때
            if (!PrDrawerSyncData.this.mSyncState.equals("play"))
                return -1.0F;

            // 첨삭을 그려줄 데이터가 없을 경우
            if ((PrDrawerSyncData.this.mSyncData == null) || (PrDrawerSyncData.this.mSyncData.size() == 0))
                return 0.0F;
            do {
                // timeDiff -> 첫번 째 그릴 도형 시간
                PrDrawerSyncData.this.timeDiff = PrDrawerSyncData.this.checkObject();
                Log.d(PrDrawerSyncData.this.TAG, "timeDiff:" + PrDrawerSyncData.this.timeDiff);
            } while (PrDrawerSyncData.this.timeDiff == 0.0F);

            return PrDrawerSyncData.this.timeDiff;
        }

        // result -> PrDrawerSyncData.this.timeDiff
        protected void onPostExecute(Float result) {
            if ((PrDrawerSyncData.this.mSyncState.equals("play")) && (PrDrawerSyncData.this.mPlayer != null)) {
                PrDrawerSyncData.this.board.setSyncInvalidateDisable(false);
                PrDrawerSyncData.this.mParent.runOnUiThread(() -> {
                    PrDrawerSyncData.this.board.invalidate();
                });
                // 첫번째 도형 시간 return 받은 후 음성파일 재생 Thread 시작!
                new SoundPlayThread().execute(new Float[]{result});
            } else {
                // play 상태가 아니고, mplayer 가 null 이 아닐경우
                PrDrawerSyncData.this.board.setSyncInvalidateDisable(false);
                PrDrawerSyncData.this.mParent.runOnUiThread(() -> {
                    Log.d("TAG", "onDraw 중!!!");
                    PrDrawerSyncData.this.board.invalidate();
                });
                if (PrDrawerSyncData.this.mSyncListener != null) {
                    PrDrawerSyncData.this.mSyncListener.onCompleteSync(0);
                }
                Log.d("TAG", "끝");
                PrDrawerSyncData.this.mSyncState = "stop";
            }
        }
    }

    /**
     * 첫번째 도형 그려지는 Start 시간 return
     *
     * @return
     */
    private float checkObject() {
        Log.d(TAG, "checkObj:" + currentNumHistory + "/" + cntHistoryCnt + ", sub:" + currentNumHistorySub + "/" + cntHistorySubCnt);
        currentNumHistory += 1;
        // 0 < 첨삭 그릴 갯수
        if (currentNumHistory < cntHistoryCnt) {
            mHistory = ((HistoryDto) mSyncData.get(currentNumHistory));
            if (mHistory.getStartTime() == 0.0F) {
                board.setSaveDraw(mHistory);
            } else {
                Log.d(TAG, "checkObj:" + mHistory.getStartTime());
                currentNumHistory -= 1;
                return mHistory.getStartTime();
            }
            return 0.0F;
        }
        return -1.0F;
    }

    private class SoundPlayThread extends AsyncTask<Float, Void, Integer> {
        private SoundPlayThread() {
        }

        protected Integer doInBackground(Float[] params) {
            // play 상태가 아니면 null return
            if (!PrDrawerSyncData.this.mSyncState.equals("play"))
                return null;

            PrDrawerSyncData.this.cntHistorySubCnt = 1;         // 움직일때마다 그려진 현재 좌표
            PrDrawerSyncData.this.currentNumHistorySub = 0;     // 총 움직일 좌표 갯수
            if ((PrDrawerSyncData.this.mPlayer != null) && (!PrDrawerSyncData.this.bAudioPlay)) {
                // Audio Play Start
                PrDrawerSyncData.this.mPlayer.start();
                PrDrawerSyncData.this.bAudioPlay = true;
                // audioPlayTime -> 현재 시간
                PrDrawerSyncData.this.audioPlayTime = System.currentTimeMillis();
            }

            // 첫번째 도형 그릴 시간 return size 1
            return params[0].intValue();
        }

        protected void onPostExecute(Integer result) {
            // result -> 첫 번째 그릴 도형 시간
            super.onPostExecute(result);
            if (result > 0) {
                Handler handler = new Handler();
                // handler -> 첫 번째 도형 그려질 시간 지나면 Thread 시작
                handler.postDelayed(() -> {
                    if (!PrDrawerSyncData.this.mSyncState.equals("play"))
                        return;
                    if ((PrDrawerSyncData.this.mSyncData != null) && (PrDrawerSyncData.this.mSyncData.size() > 0))
                        new SetProgressTaskOtherThread().execute();
                }, result);
            } else {
                if (!PrDrawerSyncData.this.mSyncState.equals("play"))
                    return;
                if ((PrDrawerSyncData.this.mSyncData != null) && (PrDrawerSyncData.this.mSyncData.size() > 0))
                    // 도형 그릴 Thread 시작
                    new SetProgressTaskOtherThread().execute();
            }
        }
    }

    private class SetProgressTaskOtherThread extends AsyncTask<Void, Void, Float> {
        private SetProgressTaskOtherThread() {
        }

        protected Float doInBackground(Void[] params) {
            // play , mPlayer not null
            if ((!PrDrawerSyncData.this.mSyncState.equals("play")) && (PrDrawerSyncData.this.mPlayer != null)) {
                return -1.0F;
            }
            // 다음 도형 그릴 시간 return
            PrDrawerSyncData.this.timeDiff = PrDrawerSyncData.this.checkObjectOthThread();
            return PrDrawerSyncData.this.timeDiff;
        }

        protected void onPostExecute(Float result) {
            if ((result != -1.0F)
                    || (PrDrawerSyncData.this.mSyncState.equals("play"))) {
                if ((PrDrawerSyncData.this.mPlayer != null)
                        && (!PrDrawerSyncData.this.bAudioPlay)) {
                    PrDrawerSyncData.this.mPlayer.start();
                    PrDrawerSyncData.this.bAudioPlay = true;
                    PrDrawerSyncData.this.mParent.runOnUiThread(() -> PrDrawerSyncData.this.board.invalidate());
                }

                if ((result > 20.0F)
                        && (result.intValue() < 2147483647)) {
                    Handler handler = new Handler();
                    handler.postDelayed(() -> new SetProgressTaskOtherThread().execute(), result.intValue());
                } else {
                    new SetProgressTaskOtherThread().execute();
                }
            }
        }
    }


    /**
     * 다음 도형 그려질 시간 Method
     *
     * @return
     */
    private float checkObjectOthThread() {
        // 시간 차
        float diff = 0.0F;
        Log.d(this.TAG, "checkObjOther:" + this.currentNumHistory + "/" + this.cntHistoryCnt + ", sub:" + this.currentNumHistorySub + "/" + this.cntHistorySubCnt);
        // currentNumHistory    -> 현재 첨삭을 그린 갯수
        // cntHistoryCnt        -> 첨삭 그릴 전체 갯수
        // cntHistorySubCnt     -> 움직일때마다 그려진 현재 좌표
        // currentNumHistorySub -> 총 움직일 좌표 갯수
        if ((this.currentNumHistory == -1) || ((this.currentNumHistory < this.cntHistoryCnt - 1) && (this.currentNumHistorySub == this.cntHistorySubCnt - 1))) {
            if (this.currentNumHistory < this.cntHistoryCnt - 1) {
                this.currentNumHistory += 1;
                this.mHistory = ((HistoryDto) this.mSyncData.get(this.currentNumHistory));
                // this.mArrXY = this.mHistory.getXyData();
                this.currentHistoryType = this.mHistory.getDrawType();
                if ((this.currentHistoryType == 222) || (this.currentHistoryType == 111)) {
                    this.cntHistorySubCnt = (this.mArrXY.size() / 3);
                    this.currentNumHistorySub = -1;
                    this.mParent.runOnUiThread(() -> PrDrawerSyncData.this.board.setDrawStart(PrDrawerSyncData.this.mHistory));
                    if (this.mArrXY.size() >= 3) {
                        diff = this.mArrXY.get(2).floatValue()
                                - (float) (System.currentTimeMillis() - this.audioPlayTime);
                    }
                    if (diff < 0.0F)
                        diff = 0.0F;
                } else if (this.currentHistoryType == 555) {
                    this.mParent.runOnUiThread(() -> PrDrawerSyncData.this.board.btnEraser(2));
                } else {
                    this.cntHistorySubCnt = 1;
                    this.currentNumHistorySub = 0;
                    Log.d("DRAW", "currentHistoryType:"
                            + this.currentHistoryType);
                    // this.mParent.runOnUiThread(() -> PrDrawerSyncData.this.board.setDrawFigure(PrDrawerSyncData.this.mHistory));
                    if (this.currentNumHistory < this.cntHistoryCnt - 1) {
                        HistoryDto nextHistory = (HistoryDto) this.mSyncData
                                .get(this.currentNumHistory + 1);

                        diff = nextHistory.getStartTime()
                                - (float) (System.currentTimeMillis() - this.audioPlayTime);
                        if (diff < 0.0F)
                            diff = 0.0F;
                    } else {
                        diff = -1.0F;
                    }
                }
            } else {
                diff = -1.0F;
            }
        } else if (this.currentNumHistorySub < this.cntHistorySubCnt - 1) {
            this.currentNumHistorySub += 1;
            Log.d("DRAW...중간", this.currentNumHistorySub + "/" + (this.cntHistorySubCnt - 1));
            this.mParent.runOnUiThread(() -> {
                if ((PrDrawerSyncData.this.currentNumHistorySub > 0)
                        && (PrDrawerSyncData.this.mArrXY.size() >= PrDrawerSyncData.this.currentNumHistorySub * 3))
                    PrDrawerSyncData.this.board.setDrawMove(PrDrawerSyncData.this.mArrXY.get(PrDrawerSyncData.this.currentNumHistorySub * 3).floatValue(),
                            PrDrawerSyncData.this.mArrXY.get(PrDrawerSyncData.this.currentNumHistorySub * 3 + 1).floatValue());
            });
            if (this.currentNumHistorySub == this.cntHistorySubCnt - 1) {
                this.mParent.runOnUiThread(() -> PrDrawerSyncData.this.board.setDrawEnd(
                        PrDrawerSyncData.this.mHistory.getEndX(),
                        PrDrawerSyncData.this.mHistory.getEndY()));
                if (this.currentNumHistory < this.cntHistoryCnt - 1) {
                    HistoryDto nextHistory = (HistoryDto) this.mSyncData
                            .get(this.currentNumHistory + 1);

                    diff = nextHistory.getStartTime()
                            - (float) (System.currentTimeMillis() - this.audioPlayTime);
                    if (diff < 0.0F)
                        diff = 0.0F;
                } else {
                    diff = -1.0F;
                }
            } else {
                diff = this.mArrXY
                        .get((this.currentNumHistorySub + 1) * 3 + 2).floatValue()
                        - (float) (System.currentTimeMillis() - this.audioPlayTime);
                if (diff < 0.0F)
                    diff = 0.0F;
            }
        } else {
            diff = -1.0F;
        }
        return diff;
    }


}

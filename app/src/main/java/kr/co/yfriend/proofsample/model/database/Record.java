package kr.co.yfriend.proofsample.model.database;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * android
 * Class: Record
 * Created by 한경동 (Joel) on 2021/10/10.
 * Description:
 */
@Entity(tableName = "record")
public class Record {

    @PrimaryKey(autoGenerate = true)
    public int id;     // id

    @ColumnInfo(name = "timer")
    public long timer;          // 영상 전체 길이

    @ColumnInfo(name = "user_name")
    public String userName;   // 유저 이름

    @ColumnInfo(name = "thumbnail")
    public String thumbNail;    // 첨삭할 이미지 ( 썸네일 )

    @ColumnInfo(name = "audio_file")
    public String audioUrl;     // 오디오 파일

    @ColumnInfo(name = "created_at")
    public String createdAt;    // 저장 날짜

    @ColumnInfo(name = "maxWidth")
    public int maxWidth;        // 뷰 영역의 최대 너비

    @ColumnInfo(name = "maxHeight")
    public int maxHeight;       // 뷰 영역의 최대 높이

    @ColumnInfo(name = "proof_history")
    public String proofHistory; // 첨삭 내용 ( JsonArray 형식 )

}

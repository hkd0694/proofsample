package kr.co.yfriend.proofsample.ui.activity;

import static kr.co.yfriend.proofsample.CommonConstant.BUCKET_ANALYZE;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Toast;

import androidx.room.Room;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import kr.co.yfriend.proofsample.R;
import kr.co.yfriend.proofsample.callback.AWSUploadCallback;
import kr.co.yfriend.proofsample.model.HistoryDto;
import kr.co.yfriend.proofsample.model.VoiceRecord;
import kr.co.yfriend.proofsample.model.database.Record;
import kr.co.yfriend.proofsample.model.database.RecordDB;
import kr.co.yfriend.proofsample.proofread.PRConfig;
import kr.co.yfriend.proofsample.proofread.PrDrawerSyncData;
import kr.co.yfriend.proofsample.ui.component.PRView;
import kr.co.yfriend.proofsample.ui.component.PRViewTest;
import kr.co.yfriend.proofsample.util.AWSUtils;
import kr.co.yfriend.proofsample.util.CaptsureUtil;
import kr.co.yfriend.proofsample.util.CommonUtil;

public class SwingVideoAnalyzeActivity extends BaseActivity implements View.OnClickListener {

    private static final String TAG = SwingVideoAnalyzeActivity.class.getSimpleName();
    private ImageView ivFrame;       // ImageView Frame

    private SeekBar seekbar;            // SeekBar

    private ViewGroup vgMiddle;

    private ImageView ivRecordPause;    // 첨삭 기록 일시정지
    private ImageView ivRecordStop;     // 첨삭 기록 초기화
    private ImageView ivRecordSave;     // 첨삭 기록 저장
    private ImageView ivRecordAdd;      // 첨삭 기록 추가

    // private PRView prPoofReadingView;   // 첨삭 View
    private PRViewTest prPoofReadingView;   // 첨삭 View
    private PrDrawerSyncData syncData;  // 첨삭 데이터
    private VoiceRecord mVoice;         // 첨삭 시작시 Audio 저장 Class
    private String mMp3Path;            // mp3 Path

    private int maxWidth;               // 첨삭 View 영역 Width
    private int maxHeight;

    private RecordDB recordDB;
    private List<Record> recordList;
    private String proofImagePath;

    private long totalTimer = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_swing_video_analyze);
        initView();
        setListener();
        // DB 관련 초기화
        recordDB = Room.databaseBuilder(getApplicationContext(), RecordDB.class, "record.db").fallbackToDestructiveMigration().allowMainThreadQueries().build();
    }


    private void initView() {
        recordList = new ArrayList<>();
        PRConfig.pathData = new ArrayList<>();
        prPoofReadingView = findViewById(R.id.pr_view_record);
        ivFrame = findViewById(R.id.iv_frame);
        vgMiddle = findViewById(R.id.vg_middle);
        ivRecordAdd = findViewById(R.id.iv_record_add);
        ivRecordPause = findViewById(R.id.iv_record_pause);   // 첨삭 기록 일시정지
        ivRecordStop = findViewById(R.id.iv_record_stop);     // 첨삭 기록 초기화
        ivRecordSave = findViewById(R.id.iv_record_save);     // 첨삭 기록 저장
        seekbar = findViewById(R.id.seekbar);
        // ViewTree의 뷰가 그려질 때마다
        vgMiddle.getViewTreeObserver()
                .addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        // FIXME : 뷰의 영역 width, height
                        maxWidth = vgMiddle.getWidth();
                        maxHeight = vgMiddle.getHeight();
                        // FIXME : 시작점도 필요할까..?
                        float x = vgMiddle.getX();
                        float y = vgMiddle.getY();
                        //리스너 해제
                        vgMiddle.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                });
    }

    private void setListener() {
        ivRecordAdd.setOnClickListener(this);
        ivRecordPause.setOnClickListener(this);
        ivRecordSave.setOnClickListener(this);
        ivRecordStop.setOnClickListener(this);
    }


    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_record_add:  // CanvasRecord -> Add Click
                showAlert("해당 이미지의 첨삭을 진행하시겠습니까?", positive -> {
                    hideViews(seekbar, ivRecordAdd);
                    proofImagePath = CaptsureUtil.captureView(vgMiddle);
                    showViews(ivRecordPause, prPoofReadingView);
                    prPoofReadingView.setPaintInit();
                    startRecord();
                }, negative -> {
                });
                break;
            case R.id.iv_record_pause:  // CanvasRecord -> Pause Click
                AudioRecordOff();
                hideViews(ivRecordPause);
                showViews(ivRecordStop, ivRecordSave);
                break;
            case R.id.iv_record_stop:
                File file = new File(mMp3Path);
                file.delete();
                prPoofReadingView.btnEraser(2);
                hideViews(ivRecordStop, ivRecordSave);
                showViews(ivRecordAdd);
                break;
            case R.id.iv_record_save:  // CanvasRecord -> Save Click
                String drawData = null;
                Log.d("SAVE", "size:" + PRConfig.pathData.size());
                try {
                    drawData = CommonUtil.getStringToSerializableObject(PRConfig.pathData);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                String finalDrawData = drawData;
                AWSUtils awsFile = new AWSUtils(getApplicationContext());
                BitmapDrawable bitmapDrawable = (BitmapDrawable) ivFrame.getDrawable();
                Bitmap resultBitmp = bitmapDrawable.getBitmap();
                awsFile.onBitmapConvertFilePathAbs(resultBitmp, 0);
                awsFile.awsUploadTransferUtility(BUCKET_ANALYZE);
                awsFile.setCallback(new AWSUploadCallback() {
                    @Override
                    public void onSuccess(String filePath, Bitmap thumbnail) {
                        // DB 저장 후 화면 이동
                        Observable.fromCallable(() -> {
                            Record record = new Record();
                            record.thumbNail = proofImagePath;
                            record.audioUrl = mMp3Path;
                            record.timer = totalTimer;
                            record.createdAt = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                            record.maxWidth = maxWidth;     // view 영역 Width
                            record.maxHeight = maxHeight;   // view 영역 Height
                            record.userName = "테스트";
                            record.proofHistory = finalDrawData;
                            recordDB.recordDao().insertAll(record);
                            recordList.add(record);
                            return true;
                        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(poseArray -> {
                            ivRecordAdd.setVisibility(View.VISIBLE);
                            ivRecordStop.setVisibility(View.GONE);
                            ivRecordSave.setVisibility(View.GONE);
                            showAlert("첨삭 기록을 완료하였습니다.", v1 -> {
                                prPoofReadingView.btnEraser(2);
                                hideViews(ivRecordStop, ivRecordSave);
                                showViews(ivRecordAdd);
                            });
                        });
                    }

                    @Override
                    public void onProgressChange() {
                    }

                    @Override
                    public void onError(Exception ex) {
                        showAlert(ex.getMessage());
                    }
                });
                break;
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        // Event Bus 등록
        if (!EventBus.getDefault().isRegistered(this)) EventBus.getDefault().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Event But 등록 해제
        if (EventBus.getDefault().isRegistered(this)) EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (PRConfig.RECORD_PLAY_STATE) {
            this.syncData.stopSyncView();
        }
        // Canvas 관련 그리기 초기화 Method
        initCanvasData();
    }

    private void initCanvasData() {
        PRConfig.pencilData = new ArrayList<>();        // 그리기 ( 펜 ) 초기화
        PRConfig.pencilUndoData = new ArrayList<>();    // 그리기 ( 펜 ) 되돌리기 초기화
        PRConfig.totalDrawData = new ArrayList<>();     // 그리기 ( 도형 ) 초기화
        PRConfig.totalDrawUndoData = new ArrayList<>(); // 그리기 ( 도형 ) 되돌리기 초기화
    }

    // CallEvent EventBus Class 등록
    public static class CallEvent {
        Record record;

        public CallEvent(Record record) {
            this.record = record;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void syncPlayRecord(CallEvent event) {
        if (event != null) {
            Record record = event.record;
            String drawData = record.proofHistory;
            String mp3Path = record.audioUrl;
        }
    }

    /**
     * 첨삭 기록 시작 Method
     */
    private void startRecord() {

        int count = PRConfig.pathData.size(); // 전체 데이터 Size

        int deleteStartIndex = -1;

        for (int i = 0; i < count; i++) {
            HistoryDto historydto = (HistoryDto) PRConfig.pathData.get(i);
            if (historydto.getStartTime() > 0.0F) {
                deleteStartIndex = i;
                break;
            }
        }
        if (deleteStartIndex != -1) {
            for (int i = count - 1; i >= deleteStartIndex; i--) {
                PRConfig.pathData.remove(i);
            }
            // 기록을 시작하기 위해 데이터를 모두 지운다.
            prPoofReadingView.btnEraser(2);
            for (int i = 0; i < PRConfig.pathData.size(); i++) {
                prPoofReadingView.setSaveDraw((HistoryDto) PRConfig.pathData.get(i));
            }
        }
        String fileName = "Sample" + "_" + new SimpleDateFormat("yyyyMMdd_HHmmssSSS").format(new Date()) + ".mp4";
        AudioRecordOn(fileName);
    }

    public void AudioRecordOn(String soundfilename) {
        this.mVoice = new VoiceRecord();
        this.mMp3Path = (getFilesDir() + File.separator + soundfilename);
        this.mVoice.setFilePath(this.mMp3Path);
        this.mVoice.startRecording();

        PRConfig.RECORD_AUDIO = true;
        totalTimer = 0;
        PRConfig.START_RECORD_TIME = System.currentTimeMillis();
    }

    public void AudioRecordOff() {
        if (this.mVoice == null)
            return;
        if (this.mVoice.finishRecording()) {
            totalTimer = System.currentTimeMillis() - PRConfig.START_RECORD_TIME;
            PRConfig.RECORD_AUDIO = false;
            PRConfig.START_RECORD_TIME = 0L;
            this.mVoice = null;
        } else {
            Toast.makeText(getApplicationContext(), "Not Record", Toast.LENGTH_SHORT).show();
        }
    }


    /**
     * 뒤로 가기
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}

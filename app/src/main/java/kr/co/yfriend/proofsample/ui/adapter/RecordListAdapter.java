package kr.co.yfriend.proofsample.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONException;

import java.util.ArrayList;

import kr.co.yfriend.proofsample.R;
import kr.co.yfriend.proofsample.model.database.Record;
import kr.co.yfriend.proofsample.ui.adapter.holder.RecordListHolder;

/**
 * ProofSample
 * Class: RecordListAdapter
 * Created by 한경동 (Joel) on 2022/03/21.
 * Description:
 */
public class RecordListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements RecordListHolder.OnItemClickListener {

    public interface OnItemClickListener {
        void onItemClick(Record record) throws JSONException;

        void onItemDelete(Record record);
    }

    private OnItemClickListener listener;

    public void setListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    private Context context;
    private ArrayList<Record> recordArrayList;

    public RecordListAdapter(Context context) {
        this.context = context;
        recordArrayList = new ArrayList<>();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.holder_record, parent, false);
        RecordListHolder holder = new RecordListHolder(view);
        holder.setListener(this);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((RecordListHolder) holder).onBind(recordArrayList.get(position));
    }

    @Override
    public int getItemCount() {
        return recordArrayList.size();
    }

    public void setData(ArrayList<Record> records) {
        this.recordArrayList = records;
        notifyDataSetChanged();
    }

    @Override
    public void onItemClick(int position) throws JSONException {
        if (listener != null) {
            listener.onItemClick(recordArrayList.get(position));
        }
    }

    @Override
    public void onDeleteClick(int position) {
        if (listener != null) {
            listener.onItemDelete(recordArrayList.get(position));
        }
    }
}


package kr.co.yfriend.proofsample.ui.component;


import static kr.co.yfriend.proofsample.proofread.PRConfig.DRAWETYPE_ANGLE;
import static kr.co.yfriend.proofsample.proofread.PRConfig.DRAWETYPE_LINE;
import static kr.co.yfriend.proofsample.proofread.PRConfig.DRAWTYPE_ERASER;
import static kr.co.yfriend.proofsample.proofread.PRConfig.DRAWTYPE_PEN;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.os.PowerManager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.Nullable;

import java.text.DecimalFormat;
import java.util.ArrayList;

import kr.co.yfriend.proofsample.model.CanvasLine;
import kr.co.yfriend.proofsample.model.HistoryDto;
import kr.co.yfriend.proofsample.proofread.PRConfig;

/**
 * androids
 * Class: PRView
 * Created by 한경동 (Joel) on 2021/10/28.
 * Description: 그림판, 도형을 그릴 View
 */
public class PRView extends View {

    private Paint mPencilPaint;     // Draw Paint
    public static Paint eraserPaint;// Eraser Paint
    public Canvas mCanvas;          // mCanvas
    Path savePath = new Path();     // savePath

    public Bitmap mBitmap;          // 그릴 이미지ㄴ
    private Bitmap previousBitmap;  // 이전 이미지

    private String mDrawType;       // Draw Type

    Context context;

    float sxTest = 0.0F;
    float syTest = 0.0F;
    float startXTest = 0.0F;
    float startYTest = 0.0F;

    private int penState = 0;
    private boolean isFinish = false;       // state = 3 부터 true

    PowerManager pm;

    PowerManager.WakeLock wl;
    private boolean isChange = false;
    private boolean isEnable = true;
    public static boolean mPathException = false;
    HistoryDto saveData = new HistoryDto();             // History 저장 VO
    private ArrayList<Double> xyList = new ArrayList();  // X, Y, 시간 저장
    private ArrayList<CanvasLine> lines = new ArrayList<>();
    public Boolean mSyncInvalidateDisable = false;
    private CanvasLine line;

    public PRView(Context context) {
        super(context);
        this.context = context;
        initView();
    }

    public PRView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        initView();
    }

    public PRView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        initView();
    }

    public void setSyncInvalidateDisable(Boolean mSyncInvalidateDisable) {
        this.mSyncInvalidateDisable = mSyncInvalidateDisable;
    }

    @SuppressLint({"InvalidWakeLockTag", "WrongConstant"})
    public void initView() {
        if (pm == null) {
            pm = ((PowerManager) context.getSystemService("power"));
        }
        if (wl == null)
            wl = pm.newWakeLock(536870922, "My Tag");
        setPaintInit();
    }

    public void setPaintInit() {

        if (mPencilPaint == null) {
            mPencilPaint = new Paint();
        }

        if (eraserPaint == null) {
            eraserPaint = new Paint(1);
            eraserPaint.setStyle(Paint.Style.STROKE);
            eraserPaint.setStrokeJoin(Paint.Join.ROUND);
            eraserPaint.setStrokeCap(Paint.Cap.ROUND);
            eraserPaint.setAntiAlias(true);
            eraserPaint.setStrokeWidth(40.0F);
            eraserPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        }

        mPencilPaint.reset();
        mPencilPaint.setStyle(Paint.Style.STROKE);
        mPencilPaint.setStrokeJoin(Paint.Join.ROUND);
        mPencilPaint.setStrokeCap(Paint.Cap.ROUND);
        mPencilPaint.setColor(Color.RED);
        mPencilPaint.setStrokeWidth(7);
        mPencilPaint.setAntiAlias(true);
        mPencilPaint.setMaskFilter(null);
        mPencilPaint.setPathEffect(null);
        mDrawType = DRAWTYPE_PEN;

    }

    public void setStrokeType(int size, int color, int type) {
        setPaintInit();
        mPencilPaint.setColor(color);
        if (type == 0) mPencilPaint.setStrokeWidth(5);
        else if (type == 1) mPencilPaint.setStrokeWidth(7);
    }

    protected void onDraw(Canvas canvas) {

        if (this.mSyncInvalidateDisable)
            return;
        if (mDrawType.equals(DRAWTYPE_ERASER)) {
            mCanvas.drawPath(savePath, mPencilPaint);
            canvas.drawBitmap(mBitmap, 0.0F, 0.0F, null);
        } else {
            canvas.drawBitmap(mBitmap, 0, 0, null);
            canvas.drawPath(savePath, mPencilPaint);
        }
    }

    // Touch Start
    private void touchStart() {
        penState = 1;
    }

    // Touch Move
    private void touchMove(float x, float y) {
        if (!this.isChange)
            this.isChange = true;

        if (penState == 1) {
            savePath.moveTo(x, y);
            sxTest = x;
            syTest = y;
        }

        savePath.quadTo(sxTest, syTest, (x + sxTest) / 2, (y + syTest) / 2);

        int iSizeWidth = 100;
        int cx = ((int) sxTest + (int) x) / 2;
        int cy = ((int) syTest + (int) y) / 2;

        if (!this.mSyncInvalidateDisable.booleanValue()) {
            invalidate(cx - iSizeWidth, cy - iSizeWidth, cx + iSizeWidth, cy + iSizeWidth);
        }

        sxTest = x;
        syTest = y;

        penState = 2;
    }

    private void touchUp(float x, float y) {

        mCanvas.drawPath(savePath, mPencilPaint);
        savePath.reset();

        penState = 3;
    }

    public void setMDrawType(String type) {
        setPaintInit();
        this.mDrawType = type;
        if (type.equals(DRAWTYPE_PEN)) {
            mPencilPaint.setStyle(Paint.Style.STROKE);
            mPencilPaint.setStrokeWidth(6);
            mPencilPaint.setColor(Color.RED);
        } else if (type.equals(DRAWTYPE_ERASER)) {
            mPencilPaint.setStyle(Paint.Style.STROKE);
            mPencilPaint.setStrokeWidth(20);
            mPencilPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (!this.isEnable)
            return true;
        Log.d("SCROLL", "X : " + event.getX() + "   Y: " + event.getY());

        float x = event.getX();
        float y = event.getY();

        setDrawingCacheEnabled(true);

        if (PRConfig.RECORD_PLAY_STATE) {
            return true;
        }

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                // 손을 터치 할때 ( 0 )
                PRConfig.undoData.clear();
                if (mDrawType.equals(DRAWTYPE_ERASER)) {
                    // 지우개 모드
                    saveData.setDrawType(111);
                    saveData.setSaveBlushSize(6);
                } else if (mDrawType.equals(DRAWTYPE_PEN)) {
                    // 그리기 모드
                    saveData.setDrawType(222);
                    saveData.setSaveBlushSize(8);
                }
                // saveData.setSaveBlushColor(Color.RED);
                saveData.setStartX(x);
                saveData.setStartY(y);

                if ((PRConfig.START_RECORD_TIME == 0L) && (!PRConfig.RECORD_AUDIO)) {
                    saveData.setStartTime(0.0F);
                } else {
                    // 기록 모드 일때 startTime 을 준다.
                    saveData.setStartTime((float) (System.currentTimeMillis() - PRConfig.START_RECORD_TIME));
                }

                if (mDrawType.equals(DRAWTYPE_PEN) || mDrawType.equals(DRAWTYPE_ERASER)) {
                    touchStart();
                } else if (mDrawType.equals(DRAWETYPE_ANGLE)) {

                } else {
                    touchStartLine(x, y);
                }

                mPathException = true;
                break;
            case MotionEvent.ACTION_MOVE:
                // 손을 움직 일때 ( 2 )
                if (mDrawType.equals(DRAWTYPE_PEN) || mDrawType.equals(DRAWTYPE_ERASER)) {
                    touchMove(x, y);
                } else if (mDrawType.equals(DRAWETYPE_ANGLE)) {
                    // 각도기 그리기
                    touchMoveLine(x, y);
                } else {
                    touchMoveLine(x, y);
                }
                line = new CanvasLine();
                line.setPointX(x);
                line.setPointY(y);
                line.setPointTime(System.currentTimeMillis() - PRConfig.START_RECORD_TIME);
                xyList.add((double) x);
                xyList.add((double) y);
                if ((PRConfig.START_RECORD_TIME == 0L) && (!PRConfig.RECORD_AUDIO)) {
                    xyList.add(0.0);
                } else {
                    // 기록 모드 일때 움직일떄마다 시간을 저장 한다.
                    double ok = (double)(System.currentTimeMillis() - PRConfig.START_RECORD_TIME);
                    DecimalFormat form = new DecimalFormat("#.00");
                    String a = form.format(ok);
                    Log.d("ㅐㅏㅐㅏ", a);
                    xyList.add(Double.parseDouble(a));
                }
                lines.add(line);
                break;
            case MotionEvent.ACTION_UP:
                // 손을 뗄때 ( 1 )
                if (mDrawType.equals(DRAWTYPE_PEN) || mDrawType.equals(DRAWTYPE_ERASER)) {
                    touchUp(x, y);
                }
                mPathException = false;
                // saveData.setXyData(xyList);
                saveData.setLines(lines);
                saveData.setEndX(x);
                saveData.setEndY(y);
                saveData.setbSync(true);
                PRConfig.pathData.add(saveData);
                xyList = new ArrayList();
                lines = new ArrayList<>();
                saveData = new HistoryDto();
                break;
        }
        return true;
    }

    /**
     * 도형 그리기 Move Start
     *
     * @param x
     * @param y
     */
    private void touchStartLine(float x, float y) {
        startXTest = x;
        startYTest = y;
        penState = 1;
    }

    /**
     * 도형 그리기 Moving
     *
     * @param x
     * @param y
     */
    private void touchMoveLine(float x, float y) {
        savePath.reset();
        savePath.moveTo(startXTest, startYTest);
        if (mDrawType.equals(DRAWETYPE_LINE)) {
            savePath.lineTo(x, y);
        }
        invalidate();
        sxTest = x;
        syTest = y;
        penState = 2;
    }


    /**
     * 도형 그리기 Move Up
     *
     * @param x
     * @param y
     */
    private void touchUpLine(float x, float y) {
        if (mDrawType.equals(DRAWETYPE_ANGLE)) {

        } else {
            mCanvas.drawPath(savePath, mPencilPaint);
            savePath.reset();
            penState = 3;
        }
    }

    public void btnEraser(int type) {
        switch (type) {
            case 2:
                savePath.reset();
                eraserPaint.setStyle(Paint.Style.FILL);
                // 전체 지우기
                this.mCanvas.drawRect(0.0F, 0.0F, getWidth(), getHeight(), eraserPaint);
                invalidate();
                break;
        }
    }

    public void setSaveDraw(HistoryDto historyDto) {
        switch (historyDto.getDrawType()) {
            case PRConfig.DRAW_PEN:
                setPaintInit();
                touchStart();
                // ArrayList<Double> xyList = historyDto.getXyData();
                // int size = xyList.size();
                // int divUnit = (historyDto.getbSync() != null) && (historyDto.getbSync()) ? 3 : 2;
                // Log.d("DRAW_PEN", "rotation size : " + xyList.size() + ":" + divUnit);
                // for (int i = 0; i < size; i += divUnit) {
                //     // 저장된 값만 해서 Draw!
                //     touchMove(xyList.get(i).floatValue(), xyList.get(i + 1).floatValue());
                // }
                // touchUp(historyDto.getEndX(), historyDto.getEndY());
                break;
            case PRConfig.DRAW_ERASER:
                setPaintInit();
                mDrawType = DRAWTYPE_ERASER;
                mPencilPaint.setStrokeWidth(20);
                mPencilPaint.setStyle(Paint.Style.STROKE);
                mPencilPaint.setXfermode(new PorterDuffXfermode(
                        PorterDuff.Mode.CLEAR));
                touchStart();
                // ArrayList<Double> xy = historyDto.getXyData();
                // int len = xy.size();
                // int unit = (historyDto.getbSync() != null) && (historyDto.getbSync()) ? 3 : 2;
                // // Log.d("DRAW_PEN", "rotation size : " + len.size() + ":" + unit);
                // for (int i = 0; i < len; i += unit) {
                //     // 저장된 값만 해서 Draw!
                //     touchMove(xy.get(i).floatValue(), xy.get(i + 1).floatValue());
                // }
                // touchUp(historyDto.getEndX(), historyDto.getEndY());
                break;
        }
    }

    public void setDrawStart(HistoryDto historyDto) {

        setPaintInit();
        touchStart();
        // ArrayList xy = historyDto.getXyData();
        // if (xy.size() > 2)
        //     touchMove(Float.parseFloat(xy.get(0).toString()), Float.parseFloat(xy.get(1).toString()));
    }

    public void setDrawMove(float nX, float nY) {
        touchMove(nX, nY);
    }

    public void setDrawEnd(float nX, float nY) {
        touchUp(nX, nY);
    }

    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        Bitmap.Config config = Bitmap.Config.ARGB_8888;
        Log.d("View SIZE CHANGE:", w + "x" + h + ":" + this.mBitmap);

        if (mBitmap == null) {
            mBitmap = Bitmap.createBitmap(w, h, config);
            if (previousBitmap != null) {
                mBitmap = Bitmap.createScaledBitmap(previousBitmap,
                        getWidth(),
                        getHeight(),
                        true);
            }
            mCanvas = new Canvas(mBitmap);
            initView();
        } else {
            mBitmap = Bitmap.createScaledBitmap(mBitmap, w, h, true);
            mCanvas = new Canvas(mBitmap);
        }
        invalidate();
    }

    public void setPreviousBitmap(Bitmap bitmap) {
        previousBitmap = bitmap;
        if (mBitmap != null) {
            mBitmap = Bitmap.createScaledBitmap(previousBitmap,
                    getWidth(),
                    getHeight(), true);
            mCanvas = new Canvas(mBitmap);
        }
        invalidate();
    }
}


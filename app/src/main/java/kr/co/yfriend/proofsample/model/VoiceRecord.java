package kr.co.yfriend.proofsample.model;

import android.media.MediaRecorder;
import android.util.Log;

import java.io.IOException;

/**
 * android
 * Class: VoiceRecord
 * Created by 한경동 (Joel) on 2021/11/03.
 * Description:
 */
public class VoiceRecord {

    private TObjectType mObjType = null;
    public String mFilePath = null;
    // private RecMicToMp3 mRecMicToMp3 = null;

    private MediaRecorder mRecorder = null;

    public static TObjectType defVoiceType = TObjectType.OT_MP4;

    public enum TObjectType {
        OT_MP3(0), OT_MP4(1), OT_3GP(2);

        private final int value;

        TObjectType(int value) {
            this.value = value;
        }

        public int value() {
            return value;
        }

        public String toString() {
            String sResult = null;

            switch (value) {
                case 0:
                    sResult = "mp3";
                    break;
                case 1:
                    sResult = "mp4";
                    break;
                case 2:
                    sResult = "3gp";
                    break;

            }

            return sResult;
        }
    }

    ;

    public VoiceRecord() {
        mObjType = defVoiceType;
    }

    public VoiceRecord(TObjectType objType) {
        mObjType = objType;
    }

    public void setFilePath(String sPath) {

        mFilePath = sPath;

        switch (mObjType) {
            case OT_MP3:
                // mRecMicToMp3 = new RecMicToMp3(mFilePath, 16000);
                break;

            default:
                mRecorder = new MediaRecorder();
                mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                switch (mObjType) {
                    case OT_3GP:
                        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
                        break;
                    case OT_MP4:
                        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
                        break;
                }

                mRecorder.setOutputFile(mFilePath);
                mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

                mRecorder.setOnErrorListener(errorListener);
                mRecorder.setOnInfoListener(infoListener);

                break;
        }
    }

    private MediaRecorder.OnErrorListener errorListener = (mr, what, extra) -> Log.i("err", "Recording : " + "Error: " + what);

    private MediaRecorder.OnInfoListener infoListener = (mr, what, extra) -> Log.i("err", "Recording : " + "Warning: " + what);

    public boolean startRecording() {
        try {
            switch (mObjType) {
                case OT_MP3:
                    break;
                default:
                    try {
                        mRecorder.prepare();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    mRecorder.start();
                    break;
            }
            Log.i("record", "startRecording");
        } catch (Exception e) {
            Log.i("err", mObjType + " // startRecording : " + e.toString());
            return false;
        }
        return true;
    }

    public boolean finishRecording() {
        try {
            switch (mObjType) {
                case OT_MP3:
                    // mRecMicToMp3.stop();
                    break;
                default:
                    if (null != mRecorder) {
                        mRecorder.stop();
                        mRecorder.reset();
                        mRecorder.release();
                        mRecorder = null;
                    }
                    break;
            }
            Log.i("record", "finishRecording");
        } catch (Exception e) {
            Log.i("err", mObjType + " // startRecording : " + e.toString());
            return false;
        }
        return true;
    }
}

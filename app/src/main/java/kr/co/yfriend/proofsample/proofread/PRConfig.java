package kr.co.yfriend.proofsample.proofread;

import java.util.ArrayList;

import kr.co.yfriend.proofsample.model.HistoryDto;


/**
 * android
 * Class: PRConfig
 * Created by 한경동 (Joel) on 2021/10/28.
 * Description:
 */
public class PRConfig {

    public static boolean RECORD_PLAY_STATE = false;    // 첨삭 기록 상태
    public static boolean RECORD_AUDIO = false;         // 오디오 상태
    public static long START_RECORD_TIME = 0L;          // 기록 시작 시간

    public static boolean RECORD_PLAYING_STATE = false; // 첨삭 기록 플레이 상태

    public static final int DRAW_ERASER = 111;      // 지우개 모드
    public static final int DRAW_PEN = 222;         // 그리기 모드
    public static final int DRAW_POLY_GONE = 444;   // 도형 그리기 모드
    public static final int DRAW_ALL_ERASER = 555;  // 모두 지우기 모드

    public static final String DRAWTYPE_PEN = "pen";     // 그리기 모드

    public static final String DRAWTYPE_ERASER = "eraser";  // 지우개 모드

    public static final String DRAWTYPE_ERASER_CLEAR = "eraserClear"; // 지우개 모두 지우기

    public static final String DRAWETYPE_LINE = "line";  // 도형 그리기 모드

    public static final String DRAWETYPE_RECTANGLE = "rectangle";

    public static final String DRAWETYPE_CIRCLE = "circle";

    public static final String DRAWETYPE_ANGLE = "angle";

    public static final String PROOF_READING_STRING = "";
    public static String PROOF_READING_THUMBNAIL = "";


    public static ArrayList<HistoryDto> pathData = new ArrayList();     // 전체 데이터
    public static ArrayList<HistoryDto> undoData = new ArrayList();     // undo 데이터

    public static ArrayList<HistoryDto> pencilData = new ArrayList<>();
    public static ArrayList<HistoryDto> pencilUndoData = new ArrayList<>();
    public static ArrayList<Integer> totalDrawData = new ArrayList<>();
    public static ArrayList<Integer> totalDrawUndoData = new ArrayList<>();

}

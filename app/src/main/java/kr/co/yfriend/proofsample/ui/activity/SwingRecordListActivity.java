package kr.co.yfriend.proofsample.ui.activity;

import android.content.Intent;
import android.graphics.Canvas;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import kr.co.yfriend.proofsample.R;
import kr.co.yfriend.proofsample.model.CanvasLine;
import kr.co.yfriend.proofsample.model.HistoryDto;
import kr.co.yfriend.proofsample.model.database.Record;
import kr.co.yfriend.proofsample.model.database.RecordDB;
import kr.co.yfriend.proofsample.ui.adapter.RecordListAdapter;
import kr.co.yfriend.proofsample.util.CommonUtil;

public class SwingRecordListActivity extends BaseActivity implements View.OnClickListener, RecordListAdapter.OnItemClickListener {

    private RecyclerView rvVideo;
    private RecordListAdapter adapter;
    private ArrayList<Record> records;

    private View vgVideo;
    private TextView tvBtnVideo;
    private TextView tvNoData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_swing_record_list);
        initView();
        setListener();
        rvVideo.setLayoutManager(new LinearLayoutManager(this));
        rvVideo.setAdapter(adapter);
        DividerItemDecoration deco = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        rvVideo.addItemDecoration(deco);
        selectListData();
    }

    private void initView() {
        vgVideo = findViewById(R.id.vg_video);
        tvBtnVideo = findViewById(R.id.tv_btn_video);
        tvNoData = findViewById(R.id.tv_no_data);
        rvVideo = findViewById(R.id.rv_video);
        records = new ArrayList<>();
        adapter = new RecordListAdapter(this);
    }

    private void setListener() {
        tvBtnVideo.setOnClickListener(this);
        adapter.setListener(this);
    }

    @Override
    public void onClick(View v) {

    }

    private void selectListData() {
        // showProgressDialog();
        Observable.fromCallable(() -> {
            RecordDB record = RecordDB.getInstance(this);
            if(record != null) records = (ArrayList<Record>) record.recordDao().getAll();
            return true;
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(isUpload -> {
            initAdapterView();
            // dismissProgressDialog();
        });
    }

    /**
     * Adapter 개수에 따라 보여지는 View 설정
     */
    private void initAdapterView() {
        if (records.size() == 0) {
            rvVideo.setVisibility(View.GONE);
            tvNoData.setVisibility(View.VISIBLE);
        } else {
            rvVideo.setVisibility(View.VISIBLE);
            tvNoData.setVisibility(View.GONE);
            adapter.setData(records);
        }
    }

    @Override
    public void onItemClick(Record record) {
        Log.d("TAG", record.toString());
        try {
            JSONObject jsonData = new JSONObject();
            jsonData.put("id", record.id);
            jsonData.put("timer", record.timer);
            jsonData.put("maxWidth",record.maxWidth);
            jsonData.put("maxHeight",record.maxHeight);
            jsonData.put("userName", record.userName);
            jsonData.put("thumbNail", record.thumbNail);
            jsonData.put("audioUrl", record.audioUrl);
            jsonData.put("createdAt", record.createdAt);
            JSONArray array = new JSONArray();
            JSONArray xyData = new JSONArray();
            ArrayList<HistoryDto> ok = (ArrayList) CommonUtil.getSerializationObjectToString(record.proofHistory);
            for(int i = 0; i<ok.size(); i++) {
                HistoryDto historyDto = ok.get(i);
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("bSync", historyDto.getbSync());
                jsonObject.put("drawType", historyDto.getDrawType());
                jsonObject.put("endTime", historyDto.getEndTime());
                jsonObject.put("endX", historyDto.getEndX());
                jsonObject.put("endY", historyDto.getEndY());
                jsonObject.put("startTime", historyDto.getStartTime());
                jsonObject.put("startX", historyDto.getStartX());
                jsonObject.put("startY", historyDto.getStartY());
                jsonObject.put("index", historyDto.getIndex());
                jsonObject.put("saveBlushColor", historyDto.getSaveBlushColor());
                jsonObject.put("saveBlushSize", historyDto.getSaveBlushSize());
                // ArrayList<Double> doubleArrayList = historyDto.getXyData();
                // String testData = "[";
                // for(int z = 0; z < doubleArrayList.size() - 1; z++) {
                //     testData += doubleArrayList.get(z) + ", ";
                // }
                // testData += doubleArrayList.get(doubleArrayList.size() - 1) + "]";
                // jsonObject.put("xyData", testData);
                for(int k = 0; k < historyDto.getLines().size(); k ++) {
                    JSONObject ok123 = new JSONObject();
                    CanvasLine line = historyDto.getLines().get(k);
                    ok123.put("pointX", line.getPointX());
                    ok123.put("pointY", line.getPointY());
                    ok123.put("pointTime", line.getPointTime());
                    xyData.put(ok123);
                }
                jsonObject.put("canvasLine", xyData);
                array.put(jsonObject);
            }
            jsonData.put("proofRead", array);
            // jsonData.put("canvasLine", xyData);
            // sdcardCertArray.add(this)
            // val listItem = CertListItem(this.index, this.user, this.issuer, this.type, this.date, this.isExpired)
            // certListJsonArray.put(JSONObject(Gson().toJson(listItem)))
            // jsonData.put("proofHistory", (ArrayList) CommonUtil.getSerializationObjectToString(record.proofHistory));
            Log.d("TAG", jsonData.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        // PROOF_READING_STRING = record.proofHistory;
        // Intent intent = new Intent(getApplicationContext(), ProofReadingActivity.class);
        Intent intent = new Intent(getApplicationContext(), ProofReadingTestActivity.class);
        intent.putExtra("timer", record.timer);
        intent.putExtra("videoUrl", record.audioUrl);
        intent.putExtra("thumbNail", record.thumbNail);
        intent.putExtra("proof", record.proofHistory);
        startActivity(intent);
    }

    @Override
    public void onItemDelete(Record record) {
        showAlert("해당 첨삭기록을 삭제하시겠습니까?", v -> {
            deleteRecordData(record);
        }, v2 -> {

        });
    }

    /**
     * DB 첨삭 기록 삭제 Method
     * @param record
     */
    private void deleteRecordData(Record record) {
        Observable.fromCallable(() -> {
            RecordDB recordDB = RecordDB.getInstance(this);
            if(recordDB != null) recordDB.recordDao().delete(record);
            return true;
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(isUpload -> {
            records.remove(record);
            initAdapterView();
            showAlert("삭제가 완료되었습니다.");
        });
    }

}

package kr.co.yfriend.proofsample;

import static android.Manifest.permission.CALL_PHONE;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;

import kr.co.yfriend.proofsample.ui.activity.BaseActivity;
import kr.co.yfriend.proofsample.ui.activity.SwingRecordListActivity;
import kr.co.yfriend.proofsample.ui.activity.SwingVideoAnalyzeActivity;
import kr.co.yfriend.proofsample.util.CommonUtil;

public class MainActivity extends BaseActivity {
    private static final String TAG = MainActivity.class.getSimpleName();

    private Button btnTest;
    private Button btnProof;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnTest = findViewById(R.id.btn_test);
        btnProof = findViewById(R.id.btn_proof);
        checkPermission();
        btnTest.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), SwingVideoAnalyzeActivity.class);
            startActivity(intent);
        });
        btnProof.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), SwingRecordListActivity.class);
            startActivity(intent);
        });
    }

    private void checkPermission() {
        if (!(CommonUtil.checkSelfPermission(this, WRITE_EXTERNAL_STORAGE) == PERMISSION_GRANTED &&
                CommonUtil.checkSelfPermission(this, CALL_PHONE) == PERMISSION_GRANTED &&
                CommonUtil.checkSelfPermission(this, RECORD_AUDIO) == PERMISSION_GRANTED &&
                CommonUtil.checkSelfPermission(this, CAMERA) == PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(this,
                    new String[]{WRITE_EXTERNAL_STORAGE, CALL_PHONE, RECORD_AUDIO, CAMERA},
                    REQUEST_PERMISSION_WRITE_STORAGE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.d(TAG, "PERMISSION_RESULT requestCode : " + requestCode);
        if (grantResults.length > 0 && REQUEST_PERMISSION_WRITE_STORAGE == requestCode) {
            if (!(PackageManager.PERMISSION_GRANTED == grantResults[0])) {
                gotoSetting();
            }
        }
        else if (grantResults.length > 0 && REQUEST_RECORD_PERMISSION == requestCode) {
            if (!(PackageManager.PERMISSION_GRANTED == grantResults[2])) {
                gotoSetting();
            }
        }
        else if (grantResults.length > 0 && REQUEST_CAMERA_PERMISSION == requestCode) {
            if (!(PackageManager.PERMISSION_GRANTED == grantResults[3])) {
                gotoSetting();
            }
        }
    }



}
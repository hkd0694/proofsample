package kr.co.yfriend.proofsample.model.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

/**
 * android
 * Class: RecordDB
 * Created by 한경동 (Joel) on 2021/10/10.
 * Description:
 */
@Database(entities = {Record.class}, version = 2)
public abstract class RecordDB extends RoomDatabase {

    private static RecordDB INSTANCE = null;

    public abstract RecordDao recordDao();

    public static RecordDB getInstance(Context context) {
        if( INSTANCE == null ) {
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                    RecordDB.class, "record.db").build();
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }

}

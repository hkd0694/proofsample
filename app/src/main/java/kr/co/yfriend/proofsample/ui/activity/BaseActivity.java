package kr.co.yfriend.proofsample.ui.activity;

import android.app.AlertDialog;
import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import kr.co.yfriend.proofsample.R;

/**
 * ProofSample
 * Class: BaseActivity
 * Created by 한경동 (Joel) on 2022/03/21.
 * Description:
 */
public class BaseActivity extends AppCompatActivity {

    private static final String TAG = BaseActivity.class.getSimpleName();
    public static final int REQUEST_PERMISSION_WRITE_STORAGE = 2;            // READ PHONE STATE 권한
    public static final int REQUEST_CAMERA_PERMISSION = 3;
    public static final int REQUEST_RECORD_PERMISSION = 4;
    public static final int REQUEST_PERMISSION_AUDIO = 5;            // READ PHONE STATE 권한
    public static final int REQUEST_SHOW_FITTING_HISTORY = 3001;
    public static final int REQUEST_SHOW_PURCHASE_HISTORY = 3002;
    public void gotoSetting() {
        showAlert("권한을 모두 허용해주셔야 앱을 사용하실 수 있습니다.", v -> {
            Intent appDetail = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + getPackageName()));
            appDetail.addCategory(Intent.CATEGORY_DEFAULT);
            appDetail.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(appDetail);
            finish();
        });
    }
    public void showAlert(String msg) {
        showAlert( msg, null, null );
    }

    public void showAlert(String msg, View.OnClickListener positiveListener) {
        showAlert( msg, positiveListener, null );
    }

    public void showAlert(String msg, final View.OnClickListener positiveListener, final View.OnClickListener negativeListener) {
        showAlert( msg, null, positiveListener, null, negativeListener );
    }

    public void showAlert(String msg, String positiveBtnStr, final View.OnClickListener positiveListener, String negativeBtnStr, final View.OnClickListener negativeListener) {
        LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService( LAYOUT_INFLATER_SERVICE );
        View dialogView = inflater.inflate( R.layout.popup_common, null );

        TextView tvTitle = dialogView.findViewById( R.id.tv_title );
        TextView tvDesc = dialogView.findViewById( R.id.tv_desc );
        TextView tvBtnPositive = dialogView.findViewById( R.id.tv_btn_positive );
        TextView tvBtnNegative = dialogView.findViewById( R.id.tv_btn_negative );

        tvDesc.setText( "" + msg );

        if ( null != positiveBtnStr ) {
            tvBtnPositive.setText( positiveBtnStr );
        }

        if ( null != negativeBtnStr ) {
            tvBtnNegative.setText( negativeBtnStr );
        }

//		AlertDialog.Builder builder = new AlertDialog.Builder( this ).setCancelable( false );
        AlertDialog.Builder builder = new AlertDialog.Builder( this );
        builder.setView( dialogView );

        final AlertDialog dialog = builder.create();

        tvBtnPositive.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( null != dialog ) {
                    dialog.dismiss();
                }

                if ( null != positiveListener ) {
                    positiveListener.onClick( v );
                }
            }
        });


        if ( null != negativeListener ) {
//			builder.setNegativeButton( "Cancel", negativeListener );
            tvBtnNegative.setVisibility( View.VISIBLE );
            tvBtnNegative.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if ( null != dialog ) {
                        dialog.dismiss();
                    }

                    if ( null != negativeListener ) {
                        negativeListener.onClick( v );
                    }
                }
            });
        } else {
            tvBtnNegative.setVisibility( View.GONE );
        }

        dialog.show();
    }


    /**
     * View 들 모두 Visible Setting
     * @param views
     */
    public void showViews(View... views) {
        for(View view : views) {
            view.setVisibility(View.VISIBLE);
        }
    }

    /**
     * View 모두 Visible Gone
     * @param views
     */
    public void hideViews(View... views) {
        for(View view : views) {
            view.setVisibility(View.GONE);
        }
    }

    /**
     * View 모두 Select False 세팅
     * @param views
     */
    public void initSelectViews(View... views) {
        for(View view : views) {
            view.setSelected(false);
        }
    }


}

package kr.co.yfriend.proofsample.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * android
 * Class: HistoryDto
 * Created by 한경동 (Joel) on 2021/10/13.
 * Description:
 */
public class HistoryDto implements Serializable {
    private static final long serialVersionUID = 7359195488425919417L;
    private String saveBlushColor;         // 색깔
    private int saveBlushSize;          // stroke Size
    private float startTime;            // 사용자가 View 영역을 최초로 터치한 시간
    private float startX;               // 사용자가 View 영역을 최초로 터치한 X 좌표
    private float startY;               // 사용자가 View 영역을 최초로 터치한 Y 좌표
    private float endTime;              // 사용자가 View 영역을 마지막으로 터치한 시간
    private float endX;                 // 사용자가 View 영역을 마지막으로 터치한 X 좌표
    private float endY;                 // 사용자가 View 영역을 마지막으로 터치한 Y 좌표
    private int drawType;               // 그리기 타입
    private int index;                  // index ( 이거 안씀 )
    private Boolean bSync;              // sync 맞추기 여부 ( 이것도 거의 안씀 )

    // lines -> X, Y, Time
    private ArrayList<CanvasLine> lines;// CanvasLine 데이터

    public int getIndex() {
        return this.index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public float getStartTime() {
        return this.startTime;
    }

    public void setStartTime(float startTime) {
        this.startTime = startTime;
    }

    public float getEndTime() {
        return this.endTime;
    }

    public void setEndTime(float endTime) {
        this.endTime = endTime;
    }

    public int getDrawType() {
        return drawType;
    }

    public void setDrawType(int drawType) {
        this.drawType = drawType;
    }

    public String getSaveBlushColor() {
        return saveBlushColor;
    }

    public void setSaveBlushColor(String saveBlushColor) {
        this.saveBlushColor = saveBlushColor;
    }

    public int getSaveBlushSize() {
        return this.saveBlushSize;
    }

    public void setSaveBlushSize(int saveBlushSize) {
        this.saveBlushSize = saveBlushSize;
    }

    public float getStartX() {
        return this.startX;
    }

    public void setStartX(float startX) {
        this.startX = startX;
    }

    public float getStartY() {
        return this.startY;
    }

    public void setStartY(float startY) {
        this.startY = startY;
    }

    public float getEndX() {
        return this.endX;
    }

    public void setEndX(float endX) {
        this.endX = endX;
    }

    public float getEndY() {
        return this.endY;
    }

    public void setEndY(float endY) {
        this.endY = endY;
    }

    public Boolean getbSync() {
        return this.bSync;
    }

    public void setbSync(Boolean bSync) {
        this.bSync = bSync;
    }

    public ArrayList<CanvasLine> getLines() {
        return lines;
    }

    public void setLines(ArrayList<CanvasLine> lines) {
        this.lines = lines;
    }
}

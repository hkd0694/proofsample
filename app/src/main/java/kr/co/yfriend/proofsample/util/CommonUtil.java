package kr.co.yfriend.proofsample.util;

import static android.content.pm.PackageManager.PERMISSION_GRANTED;

import android.content.Context;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * ProofSample
 * Class: CommonUtil
 * Created by 한경동 (Joel) on 2022/03/21.
 * Description:
 */
public class CommonUtil {

    // public static List<RecordList> saveVideoList = new ArrayList<>();

    public static int checkSelfPermission(@NonNull Context context, @NonNull String permission ) {
        if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ) {
            return ContextCompat.checkSelfPermission( context, permission );
        }

        return PERMISSION_GRANTED;
    }


    public static String getDateFormatToString(String dt, int index) {
        String result = "";
        String splitString;
        if( index == 0 ) {
            splitString = dt.split("_")[0];
            String year = splitString.substring(0,4);
            String month = splitString.substring(4,6);
            String days = splitString.substring(6,8);
            result = year + "-" + month + "-" +  days;
        } else {
            splitString = dt.split("_")[1];
            String hours = splitString.substring(0,2);
            String minutes = splitString.substring(2,4);
            String seconds = splitString.substring(4,6);
            result = hours + ":" + minutes + ":" + seconds;
        }
        return result;
    }

    /**
     * Serializable -> String 변경
     *
     * @param obj
     * @return
     * @throws Exception
     */
    public static String getStringToSerializableObject(Serializable obj) throws Exception {
        if (obj == null)
            return "";
        try {
            ByteArrayOutputStream serialObj = new ByteArrayOutputStream();
            ObjectOutputStream objStream = new ObjectOutputStream(serialObj);
            objStream.writeObject(obj);
            objStream.close();

            StringBuffer strBuf = new StringBuffer();
            byte[] bytes = serialObj.toByteArray();
            for (int i = 0; i < bytes.length; i++) {
                strBuf.append((char) ((bytes[i] >> 4 & 0xF) + 97));
                strBuf.append((char) ((bytes[i] & 0xF) + 97));
            }
            return strBuf.toString();

        } catch (Exception e) {
        }
        throw new Exception();
    }

    public static Object getSerializationObjectToString(String str) throws Exception {

        if ((str == null) || (str.length() == 0))
            return null;
        try {
            byte[] bytes = new byte[str.length() / 2];
            for (int i = 0; i < str.length(); i += 2) {
                char c = str.charAt(i);
                bytes[(i / 2)] = ((byte) (c - 'a' << 4));
                c = str.charAt(i + 1);
                int tmp44_43 = (i / 2);
                byte[] tmp44_40 = bytes;
                tmp44_40[tmp44_43] = ((byte) (tmp44_40[tmp44_43] + (c - 'a')));
            }
            ByteArrayInputStream serialObj = new ByteArrayInputStream(bytes);
            ObjectInputStream objStream = new ObjectInputStream(serialObj);
            return objStream.readObject();
        } catch (Exception e) {
        }
        throw new Exception();
    }


}
